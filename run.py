import time
import datetime
import traceback
import math
import os
import torch
import torchvision
import torch.nn as nn
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import SubsetRandomSampler
from utils import *
from dataset import DatasetQMDCT
from network import JitoNet
import numpy as np
from sklearn.metrics import f1_score
import csv

algorithm_classes = ('HCM-EECS', 'MP3_Stego', 'Negative')
length_classes = ('0','1-10','11-20','21-30','31-40','41-50','51-60','61-70','71-80','81-90','91-100')

def save_checkpoint(state, filename, epoch_num):
  torch.save(state, filename.split('.')[0] + '-' + str(epoch_num) + '.pth')
  print('Checkpoint {} Saved'. format(epoch_num))

def load_data(csv, batch_size=16, validation_split=0.2, test_split=0.2, shuffle_dataset=False, random_seed=42):
  dataset_QMDCT =  DatasetQMDCT(csv)
  len(dataset_QMDCT)

  test_length = round(len(dataset_QMDCT) * test_split)
  train_val_length = len(dataset_QMDCT) - test_length
  val_length = round(train_val_length * validation_split)
  train_length = train_val_length - val_length

  dataset_size = len(dataset_QMDCT)
  indices = list(range(dataset_size))

  if shuffle_dataset :
      np.random.seed(random_seed)
      np.random.shuffle(indices)
      
  test_indices, train_val_indices = indices[:test_length], indices[test_length:]
  train_indices, val_indices = train_val_indices[:train_length], indices[train_length:]

  train_sampler = SubsetRandomSampler(train_indices)
  valid_sampler = SubsetRandomSampler(val_indices)
  test_sampler = SubsetRandomSampler(test_indices)

  train_loader = torch.utils.data.DataLoader(dataset_QMDCT, batch_size=batch_size, sampler=train_sampler)
  validation_loader = torch.utils.data.DataLoader(dataset_QMDCT, batch_size=batch_size, sampler=valid_sampler)
  test_loader = torch.utils.data.DataLoader(dataset_QMDCT, batch_size=batch_size, sampler=test_sampler)

  return train_loader, validation_loader, test_loader

def train(device, model, train_loader, validation_loader, num_epochs=10, start_epoch=0, filename='Default.pth', is_algo=True, optimizer='adam'):
  header = ('Epoch', 'Step', 'Accuracy', 'Loss', 'Mode')
  f = open('accuracy-'+ filename +'.csv', 'w') 
  writer = csv.writer(f)
  writer.writerow(header)
  
  total_step = len(train_loader)
  total_step_val = len(validation_loader)

  train_correct, train_total = 0, 0
  val_correct, val_total = 0, 0

  learning_rate = 0.001
  criterion = nn.CrossEntropyLoss()

  if optimizer == 'adam':
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
  elif optimizer == 'SGD':
    optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
  elif optimizer == 'Adamax':
    optimizer = torch.optim.Adamax(model.parameters())
  elif optimizer == 'Adagrad':
    optimizer = torch.optim.Adagrad(model.parameters())
  elif optimizer == 'RMSProp':
    optimizer = torch.optim.RMSprop(model.parameters())
  else :
    optimizer = torch.optim.Adadelta(model.parameters(), lr=1.0, rho=0.9, eps=1e-06, weight_decay=0)

  for epoch in range(start_epoch, num_epochs):
    model.train()
    for index_train, data in enumerate(train_loader):
        QMDCT, algorithm_labels, length_labels = data

        QMDCT = QMDCT.to(device)
        algorithm_labels = torch.LongTensor(algorithm_labels).to(device)
        length_labels = torch.LongTensor(length_labels).to(device)

        outputs = model(QMDCT)

        if is_algo:
          loss = criterion(outputs, algorithm_labels)
        else:
          loss = criterion(outputs, length_labels)
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        _, predictions = torch.max(outputs.data, 1)
        if is_algo:
          train_total += algorithm_labels.size(0)
          train_correct += (predictions == algorithm_labels).sum().item()
        else:
          train_total += length_labels.size(0)
          train_correct += (predictions == length_labels).sum().item()

        train_accuracy = 100 * train_correct/train_total

        if is_algo:
          f1_score_label = f1_score(predictions.data.cpu().numpy(), algorithm_labels.data.cpu().numpy(), average='weighted') * 100
        else:
          f1_score_label = f1_score(predictions.data.cpu().numpy(), length_labels.data.cpu().numpy(), average='weighted') * 100
        
        print ('Epoch [{}/{}], Step [{}/{}], Acc: {}%, Loss: {:.4f}, F1-Score:{}%'.format(epoch+1, num_epochs, index_train+1, total_step, train_accuracy, loss.item(), f1_score_label))
      
        writer.writerow((epoch+1, index_train+1, train_accuracy, loss.data.cpu().numpy(), 'Train'))

    model.eval()
    with torch.no_grad():
        for index_val, data_val in enumerate(validation_loader):
            QMDCT, algorithm_labels, length_labels = data_val
            
            QMDCT = QMDCT.to(device)
            algorithm_labels = torch.LongTensor(algorithm_labels).to(device)
            length_labels = torch.LongTensor(length_labels).to(device)

            outputs = model(QMDCT)
            if is_algo:
              loss = criterion(outputs, algorithm_labels)
            else:
              loss = criterion(outputs, length_labels)

            _, predictions = torch.max(outputs.data, 1)
            if is_algo:
              val_total += algorithm_labels.size(0)
              val_correct += (predictions == algorithm_labels).sum().item()
            else:
              val_total += length_labels.size(0)
              val_correct += (predictions == length_labels).sum().item()

            val_accuracy = 100 * val_correct/val_total

            if is_algo:
              f1_score_label = f1_score(predictions.data.cpu().numpy(), algorithm_labels.data.cpu().numpy(), average='weighted') * 100
            else:
              f1_score_label = f1_score(predictions.data.cpu().numpy(), length_labels.data.cpu().numpy(), average='weighted') * 100
            
            print ('Epoch [{}/{}], Step [{}/{}], Val Acc: {}%, Loss: {:.4f}, F1-Score:{}%'.format(epoch+1, num_epochs, index_val+1, total_step_val, val_accuracy, loss.item(), f1_score_label))

            writer.writerow((epoch+1, index_val+1, val_accuracy, loss.data.cpu().numpy(), 'Validation'))

    state = {
      'epoch': epoch + 1,
      'classifier' : 'algorithm' if is_algo else 'length',
      'state_dict': model.state_dict(),
      'val_accuracy': val_accuracy,
      'train_accuracy' : train_accuracy,
      'loss' : loss,
      'optimizer' : optimizer.state_dict(),
    }

    save_checkpoint(state, filename, epoch+1)

  torch.save(model, filename)
  print('Model Saved')

  f.close()

  return model
  
def test(device, test_loader, model, is_algo=True):
  print('Model Loaded for Testing')

  total_step_test = len(test_loader)

  class_correct = list(0 for i in range(len(length_classes)))
  class_total = list(0 for i in range(len(length_classes)))

  f1_total = 0

  model.eval()
  with torch.no_grad():
      correct = 0
      total = 0
      for index_test, data_test in enumerate(test_loader):
          QMDCT, algorithm_labels, length_labels = data_test

          QMDCT = QMDCT.to(device)
          algorithm_labels = torch.LongTensor(algorithm_labels).to(device)
          length_labels = torch.LongTensor(length_labels).to(device)

          outputs = model(QMDCT)

          _, predicted = torch.max(outputs.data, 1)

          if is_algo:
            total += algorithm_labels.size(0)
            correct += (predicted == algorithm_labels).sum().item()
            class_correct_pred = (predicted == algorithm_labels).squeeze()

            if len(class_correct_pred.size()) != 0:
              for i in range(algorithm_labels.size(0)):
                label = length_labels[i]
                class_correct[label] += class_correct_pred[i].item()
                class_total[label] += 1
          else:
            total += length_labels.size(0)
            correct += (predicted == length_labels).sum().item()
            class_correct_pred = (predicted == length_labels).squeeze()

            if len(class_correct_pred.size()) != 0:
              for i in range(length_labels.size(0)):
                label = length_labels[i]
                class_correct[label] += class_correct_pred[i].item()
                class_total[label] += 1

          accuracy = 100 * correct / total

          if is_algo:
            f1_score_label = f1_score(predicted.data.cpu().numpy(), algorithm_labels.data.cpu().numpy(), average='weighted') * 100
            f1_total += f1_score_label
          else:
            f1_score_label = f1_score(predicted.data.cpu().numpy(), length_labels.data.cpu().numpy(), average='weighted') * 100
            f1_total += f1_score_label

          print ('Step [{}/{}], Test Acc: {}%, F1-Score: {}%'.format(index_test+1, total_step_test, accuracy, f1_score_label))

      print('Test Accuracy of the model: {} %'.format(100 * correct / total))
      print('F1-Score of the model: {} %'.format(f1_total / total_step_test))

      if is_algo:
        for i in range(len(algorithm_classes)):
          print('Accuracy of %5s : %2d %%' % (algorithm_classes[i], 100 * class_correct[i] / class_total[i]))
      else: 
        for i in range(len(length_classes)):
          print('Accuracy of %5s bytes : %2d %%' % (length_classes[i], 100 * class_correct[i] / class_total[i]))

  return (100 * correct / total)

def test_per_epoch(device, filename, test_loader, num_epochs=5, is_algo=True):
  header = ('Epoch', 'Accuracy', 'Mode')
  f = open('accuracy-test-'+ filename +'.csv', 'w') 
  writer = csv.writer(f)
  writer.writerow(header)

  model_filename = filename.split('.')[0]

  if is_algo:
    model = JitoNet(class_num=3).to(device)
  else:
    model = JitoNet(class_num=11).to(device)

  for epoch in range(1, num_epochs+1):
      print('Epoch {}'.format(epoch))
      model.load_state_dict(torch.load(model_filename + '-' + str(epoch) + '.pth')['state_dict'])
      acc = test(device, test_loader, model, is_algo)
      writer.writerow((epoch, acc, 'Test'))

  f.close