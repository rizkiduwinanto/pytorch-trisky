import os
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

from run import *
import torch
import torchvision
import torch.nn as nn
from torchvision import transforms
import numpy as np
import pandas as pd
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import SubsetRandomSampler
from scipy import ndimage

if __name__ == "__main__":
  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
  print(device)
  PATH = 'AlgorithmClassifierSGD.pth'

  header = ('Epoch', 'Accuracy', 'Mode')
  f = open('accuracy-test-'+ PATH +'.csv', 'w') 
  writer = csv.writer(f)
  writer.writerow(header)

  model_filename = PATH.split('.')[0]
  num_epochs = 5
  train_loader, validation_loader, test_loader = load_data('datasetLabel.csv', batch_size=10)

  model = JitoNet(class_num=3).to(device)

  for epoch in range(1, num_epochs+1):
    print('Epoch {}'.format(epoch))
    model.load_state_dict(torch.load(model_filename + '-' + str(epoch) + '.pth')['state_dict'])
    acc = test(device, test_loader, model, is_algo=True)
    writer.writerow((epoch, acc, 'Test'))

  f.close