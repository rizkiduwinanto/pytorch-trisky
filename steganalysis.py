import os
from run import *
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms
import numpy as np
import pandas as pd
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import SubsetRandomSampler
from scipy import ndimage

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def steganalysis_one(filename):
  ext = filename.split('.')[-1]

  if ext == 'txt':
    QMDCT = read_QMDCT(filename)
  elif ext == 'mp3':
    QMDCT = qmdct_extractor(filename)
  else:
    print('error')

  torch.nn.Module.dump_patches = True

  pred_transforms = transforms.ToTensor()
  QMDCT_tensor = pred_transforms(QMDCT).float()
  QMDCT_tensor = QMDCT_tensor.unsqueeze_(0)

  input = QMDCT_tensor
  input = input.cpu()

  model1 = torch.load('AlgorithmClassifierBest.pth', map_location=lambda storage, location: storage)
  print('Model Algorithm Loaded')

  model1.eval()
  outputs1 = model1(input)
  softmax1 = F.softmax(outputs1, dim = 1)
  softmax1 = softmax1.data.numpy()
  score_algorithm, predicted_algorithm = torch.max(outputs1.data.cpu(), 1)

  model2 = torch.load('LengthClassifierBest.pth', map_location=lambda storage, location: storage)
  print('Model Length Loaded')

  model2.eval()
  outputs2 = model2(input)
  softmax2 = F.softmax(outputs2, dim = 1)
  softmax2 = softmax2.data.numpy()
  score_length, predicted_length = torch.max(outputs2.data.cpu(), 1)

  algorithm_pred = algorithm_classes[predicted_algorithm]
  len_pred = length_classes[predicted_length]

  print('Algorithm : {}, Prob : {}%'.format(algorithm_pred, softmax1[0][predicted_algorithm] * 100))
  print('Length : {}, Prob : {}%'.format(len_pred, softmax2[0][predicted_length] * 100))

if __name__ == "__main__":
  steganalysis_one('QMDCT/wav_10s_1570_3.txt')