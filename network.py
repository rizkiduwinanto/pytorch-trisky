import os
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F

class JitoNet(nn.Module):
    def __init__(self, init=1, class_num=3):
        super(JitoNet, self).__init__()

        self.layer_input = nn.Sequential(
            nn.Conv2d(init, 16, (1, 1), stride=(1, 1), padding=(0,0), groups=init)
        )
    
        self.layer1 = nn.Sequential(
            nn.Conv2d(16, 32, (3, 3), stride=(1, 1), padding=(0,0), groups=16),
            nn.Tanh(),
            nn.Conv2d(32, 32, (1, 1), stride=(1, 1), padding=(0,0), groups=32),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, (3, 3), stride=(1, 1), padding=(0,0), groups=32),
            nn.Tanh(),
            nn.Conv2d(64, 64, (1, 1), stride=(1, 1), padding=(0,0), groups=64),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer3 = nn.Sequential(
            nn.Conv2d(64, 128, (3, 3), stride=(1, 1), padding=(0,0), groups=64),
            nn.Tanh(),
            nn.Conv2d(128, 128, (1, 1), stride=(1, 1), padding=(0,0), groups=128),
            nn.Tanh(),
            nn.MaxPool2d((2, 2),padding=1, stride=(2, 2))
        )
        
        self.layer4 = nn.Sequential(
            nn.Conv2d(128, 256, (3, 3), stride=(1, 1), padding=(0,0), groups=128),
            nn.Tanh(),
            nn.Conv2d(256, 256, (1, 1), stride=(1, 1), padding=(0,0), groups=256),
            nn.BatchNorm2d(256),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer5 = nn.Sequential(
            nn.Conv2d(256, 512, (3, 3), stride=(1, 1), padding=(0,0), groups=256),
            nn.Tanh(),
            nn.Conv2d(512, 512, (1, 1), stride=(1, 1), padding=(0,0), groups=512),
            nn.BatchNorm2d(512),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer6 = nn.Sequential(
            nn.Conv2d(512, 1024, (3, 3), stride=(1, 1), padding=(0,0), groups=512),
            nn.Tanh(),
            nn.Conv2d(1024, 1024, (1, 1), stride=(1, 1), padding=(0,0), groups=1024),
            nn.BatchNorm2d(1024),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer7 = nn.Sequential(
            nn.Linear(1024*9*3, 4096),
            nn.BatchNorm1d(4096),
            nn.Tanh(),
        )
        
        self.layer8 = nn.Sequential(
            nn.Linear(4096, 512),
            nn.BatchNorm1d(512),
            nn.Tanh(),
        ) 
        
        self.layer_output = nn.Sequential(
            nn.Linear(512, class_num)
        )
    
    def forward(self, input_data):
        x = input_data.float()
        # Filter = torch.Tensor([[[[1, 0, 0]], [[0, 1, 0]], [[0, 0, 1]]]])
        # x = F.conv2d(x, Filter, stride=(1, 1), padding=(0,0))
        x = self.layer_input(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.layer5(x)
        x = self.layer6(x)
        x = x.view(-1, 9*3*1024)
        x = self.layer7(x)
        x = self.layer8(x)
        out = self.layer_output(x)
        return out