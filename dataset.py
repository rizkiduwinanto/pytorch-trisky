from torchvision import transforms
import numpy as np
import pandas as pd
from utils import read_QMDCT
from torch.utils.data.dataset import Dataset
from scipy import ndimage

class DatasetQMDCT(Dataset):
    def __init__(self, csv_path):
        self.to_tensor = transforms.ToTensor()
        self.dataset = pd.read_csv(csv_path, header=None, skiprows=1)
        self.filename_arr = np.asarray(self.dataset.iloc[:, 1])
        self.algorithm_arr = np.asarray(self.dataset.iloc[:, 2])
        self.length_arr = np.asarray(self.dataset.iloc[:, 6])
        self.data_len = len(self.dataset.index)
        
    def __getitem__(self, index):
        MP3_filename = self.filename_arr[index]
        algorithm_label = int(self.algorithm_arr[index])
        length_label = int(self.length_arr[index])
        QMDCT_filename = MP3_filename.split('.')[0] + '.txt'
        QMDCT = read_QMDCT('../QMDCT/' + QMDCT_filename)
        return (self.to_tensor(QMDCT), algorithm_label, length_label)
        
    def __len__(self):
        return self.data_len