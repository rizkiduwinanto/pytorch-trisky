import os
import torch
import torchvision
import torch.nn as nn
from torchvision import transforms
import numpy as np
import pandas as pd
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import SubsetRandomSampler
from scipy import ndimage

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

print(device)

def read_QMDCT(text_file_path, height=200, width=576, channel=1, separator=","):
    content = []
    try:
        with open(text_file_path) as file:
            lines = file.readlines()
            for line in lines:
                try:
                    numbers = [int(character) for character in line.split(separator)[:-1]]
                except ValueError:
                    numbers = [float(character) for character in line.split(separator)[:-1]]
                content.append(numbers)

            content = np.array(content)
            [h, w] = np.shape(content)
            height_new = None if h < height else height
            width_new = None if w < width else width
            if channel == 0:
                content = content[:height_new, :width_new]
            else:
                content = np.reshape(content, [h, w, channel])
                content = content[:height_new, :width_new, :channel]
                
    except ValueError:
        print("Error read: %s" % text_file_path)
        
    return content

class DatasetQMDCT(Dataset):
    def __init__(self, csv_path):
        self.to_tensor = transforms.ToTensor()
        self.dataset = pd.read_csv(csv_path, header=None)
        self.filename_arr = np.asarray(self.dataset.iloc[:, 1])
        self.algorithm_arr = np.asarray(self.dataset.iloc[:, 2])
        self.length_arr = np.asarray(self.dataset.iloc[:, 3])
        self.data_len = len(self.dataset.index)
        
    def __getitem__(self, index):
        MP3_filename = self.filename_arr[index]
        algorithm_label = int(self.algorithm_arr[index])
        length_label = int(self.length_arr[index])
        QMDCT_filename = MP3_filename.split('.')[0] + '.txt'
        QMDCT = read_QMDCT('QMDCT/' + QMDCT_filename)
        return (self.to_tensor(QMDCT), algorithm_label, length_label)
        
    def __len__(self):
        return self.data_len

dataset_QMDCT =  DatasetQMDCT('datasetLabel.csv')
len(dataset_QMDCT)

test_length = len(dataset_QMDCT) // 5
train_val_length = len(dataset_QMDCT) - test_length
val_length = train_val_length // 5
train_length = train_val_length - val_length

test_length, train_length, val_length

batch_size = 8
validation_split, test_split = .2, .2
shuffle_dataset = True
random_seed= 42

dataset_size = len(dataset_QMDCT)
indices = list(range(dataset_size))

if shuffle_dataset :
    np.random.seed(random_seed)
    np.random.shuffle(indices)
    
test_indices, train_val_indices = indices[:test_length], indices[test_length:]
train_indices, val_indices = train_val_indices[:train_length], indices[train_length:]

train_sampler = SubsetRandomSampler(train_indices)
valid_sampler = SubsetRandomSampler(val_indices)
test_sampler = SubsetRandomSampler(test_indices)

train_loader = torch.utils.data.DataLoader(dataset_QMDCT, batch_size=batch_size, sampler=train_sampler)
validation_loader = torch.utils.data.DataLoader(dataset_QMDCT, batch_size=batch_size, sampler=valid_sampler)
test_loader = torch.utils.data.DataLoader(dataset_QMDCT, batch_size=batch_size, sampler=test_sampler)

algorithm_classes = ('HCM-EECS', 'MP3_Stego', 'Negative')
length_classes = ('0','10','20','30','40','50','60','70','80','90','100')

class JitoNet(nn.Module):
    def __init__(self, init=1, class_num=3):
        super(JitoNet, self).__init__()
        
        self.layer_input = nn.Sequential(
            nn.Conv2d(init, 16, (1, 1), stride=(1, 1), padding=(0,0), groups=init)
        )
    
        self.layer1 = nn.Sequential(
            nn.Conv2d(16, 32, (3, 3), stride=(1, 1), padding=(0,0), groups=16),
            nn.Tanh(),
            nn.Conv2d(32, 32, (1, 1), stride=(1, 1), padding=(0,0), groups=32),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, (3, 3), stride=(1, 1), padding=(0,0), groups=32),
            nn.Tanh(),
            nn.Conv2d(64, 64, (1, 1), stride=(1, 1), padding=(0,0), groups=64),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer3 = nn.Sequential(
            nn.Conv2d(64, 128, (3, 3), stride=(1, 1), padding=(0,0), groups=64),
            nn.Tanh(),
            nn.Conv2d(128, 128, (1, 1), stride=(1, 1), padding=(0,0), groups=128),
            nn.Tanh(),
            nn.MaxPool2d((2, 2),padding=1, stride=(2, 2))
        )
        
        self.layer4 = nn.Sequential(
            nn.Conv2d(128, 256, (3, 3), stride=(1, 1), padding=(0,0), groups=128),
            nn.Tanh(),
            nn.Conv2d(256, 256, (1, 1), stride=(1, 1), padding=(0,0), groups=256),
            nn.BatchNorm2d(256),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer5 = nn.Sequential(
            nn.Conv2d(256, 512, (3, 3), stride=(1, 1), padding=(0,0), groups=256),
            nn.Tanh(),
            nn.Conv2d(512, 512, (1, 1), stride=(1, 1), padding=(0,0), groups=512),
            nn.BatchNorm2d(512),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer6 = nn.Sequential(
            nn.Conv2d(512, 1024, (3, 3), stride=(1, 1), padding=(0,0), groups=512),
            nn.Tanh(),
            nn.Conv2d(1024, 1024, (1, 1), stride=(1, 1), padding=(0,0), groups=1024),
            nn.BatchNorm2d(1024),
            nn.Tanh(),
            nn.MaxPool2d((2, 2), padding=1, stride=(2, 2))
        )
        
        self.layer7 = nn.Sequential(
            nn.Linear(1024*9*3, 4096),
            nn.BatchNorm1d(4096),
            nn.Tanh(),
        )
        
        self.layer8 = nn.Sequential(
            nn.Linear(4096, 512),
            nn.BatchNorm1d(512),
            nn.Tanh(),
        ) 
        
        self.layer_output = nn.Sequential(
            nn.Linear(512, class_num)
        )
    
    def forward(self, input_data):
        x = input_data.float()
        x = self.layer_input(x)
        # kernel = torch.FloatTensor([[[0.006, 0.061, 0.242, 0.383, 0.242, 0.061, 0.006]]])
        # x = nn.Conv2d(x, kernel)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.layer5(x)
        x = self.layer6(x)
        x = x.view(-1, 9*3*1024)
        x = self.layer7(x)
        x = self.layer8(x)
        out = self.layer_output(x)
        return out
    
model = JitoNet().to(device)

learning_rate = 0.001
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

num_epochs = 2

train_correct, train_total = 0, 0
val_correct, val_total = 0, 0
running_loss = 0

total_step = len(train_loader)
total_step_val = len(validation_loader)
total_step_test = len(test_loader)

for epoch in range(num_epochs):
    for index_train, data in enumerate(train_loader):
        QMDCT, algorithm_labels, length_labels = data

        QMDCT = QMDCT.to(device)
        algorithm_labels = torch.LongTensor(algorithm_labels).to(device)
        length_labels = torch.LongTensor(length_labels).to(device)

        outputs = model(QMDCT)
        loss = criterion(outputs, algorithm_labels)
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        score, predictions = torch.max(outputs.data, 1)
        train_total += algorithm_labels.size(0)
        train_correct += (predictions == algorithm_labels).sum().item()

        train_accuracy = 100 * train_correct/train_total
        
        running_loss += loss.item()
        print ('Epoch [{}/{}], Step [{}/{}], Acc: {}, Loss: {:.4f}'.format(epoch+1, num_epochs, index_train+1, total_step, train_accuracy, loss.item()))
    
    with torch.no_grad():
        for index_val, data_val in enumerate(validation_loader):
            QMDCT, algorithm_labels, length_labels = data_val

            QMDCT = QMDCT.to(device)
            algorithm_labels = torch.LongTensor(algorithm_labels).to(device)
            length_labels = torch.LongTensor(length_labels).to(device)

            outputs = model(QMDCT)
            loss = criterion(outputs, algorithm_labels)

            score, predictions = torch.max(outputs.data, 1)
            val_total += algorithm_labels.size(0)
            val_correct += (predictions == algorithm_labels).sum().item()

            val_accuracy = 100 * val_correct/val_total
            
            print ('Epoch [{}/{}], Step [{}/{}], Val Acc: {}, Loss: {:.4f}'.format(epoch+1, num_epochs, index_val+1, total_step_val, val_accuracy, loss.item()))

torch.save(model,  './'+ name +'.pth')
print('Model Saved')

model.eval()
with torch.no_grad():
    correct = 0
    total = 0
    for index_test, data_test in enumerate(test_loader):
        QMDCT, algorithm_labels, length_labels = data_test

        QMDCT = QMDCT.to(device)
        algorithm_labels = torch.LongTensor(algorithm_labels).to(device)
        length_labels = torch.LongTensor(length_labels).to(device)

        outputs = model(QMDCT)

        _, predicted = torch.max(outputs.data, 1)
        total += algorithm_labels.size(0)
        correct += (predicted == algorithm_labels).sum().item()

        accuracy = 100 * correct / total

        print ('Step [{}/{}], Test Acc: {}'.format(index_test+1, total_step_test, accuracy))

    print('Test Accuracy of the model: {} %'.format(100 * correct / total))