import os
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

from run import *
import torch
import torchvision
import torch.nn as nn
from torchvision import transforms
import numpy as np
import pandas as pd
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import SubsetRandomSampler
from scipy import ndimage

if __name__ == "__main__":
  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
  print(device)

  print('Model for Length')

  train_loader, validation_loader, test_loader = load_data('datasetLabelBalanced.csv', batch_size=4, shuffle_dataset=True)

  model = JitoNet(class_num=11).to(device)

  fname = 'LengthClassifierAdam.pth'

  model_trained = torch.load(fname)

  # model_trained = train(device, model, train_loader, validation_loader, num_epochs=20, filename=fname, is_algo=False, optimizer='adam')
  test_per_epoch(device, fname, test_loader, num_epochs=20, is_algo=False)
  test(device, test_loader, model_trained, is_algo=False)

  print('Model for Algorithm')
  train_loader, validation_loader, test_loader = load_data('datasetLabel.csv', batch_size=10)
  
  model = JitoNet(class_num=3).to(device)
  fname = 'AlgorithmClassifierAdam2.pth'
  epoch = 7
  model.load_state_dict(torch.load(fname.split('.')[0] + '-' + str(epoch) + '.pth')['state_dict'])

  model_trained = train(device, model, train_loader, validation_loader, num_epochs=20, start_epoch=epoch+1, filename=fname, is_algo=True, optimizer='Adam')
  test_per_epoch(device, fname, test_loader, num_epochs=20, is_algo=True)
  test(device, test_loader, model_trained, is_algo=True)