import numpy as np
import os
import torch
import torchvision
import torch.nn as nn

def read_QMDCT(text_file_path, height=200, width=576, channel=1, separator=","):
    content = []
    try:
        with open(text_file_path) as file:
            lines = file.readlines()
            for line in lines:
                try:
                    numbers = [int(character) for character in line.split(separator)[:-1]]
                except ValueError:
                    numbers = [float(character) for character in line.split(separator)[:-1]]
                content.append(numbers)

            content = np.array(content)
            [h, w] = np.shape(content)
            height_new = None if h < height else height
            width_new = None if w < width else width
            if channel == 0:
                content = content[:height_new, :width_new]
            else:
                content = np.reshape(content, [h, w, channel])
                content = content[:height_new, :width_new, :channel]
                
    except ValueError:
        print("Error read: %s" % text_file_path)
        
    return content

def qmdct_extractor(mp3_file_path, width=576, frame_num=50, coeff_num=576):
    wav_file_path = mp3_file_path.replace(".mp3", ".wav")
    txt_file_path = mp3_file_path.replace(".mp3", ".txt")
    command = "wine /home/rizkiduwinanto/Documents/TRISKY/Eksperimen/python_scripts/tools/lame_qmdct.exe " + mp3_file_path + " -framenum " + str(frame_num) + " -startind 0 " + " -coeffnum " + str(coeff_num) + " --decode"
    os.system(command)
    height = frame_num * 4
    content = read_QMDCT(text_file_path=txt_file_path, height=height, width=width)
    os.remove(txt_file_path)
    os.remove(wav_file_path)
    return content
